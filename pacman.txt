# pacman.txt
# Contains basic apt programs to install

# Utils
net-tools
curl
wget
scrot
neofetch
#pulseaudio
#pulseaudio-alsa
pipewire-pulse
thunar
unzip
cronie

# Security/Privacy
veracrypt

# Terminal
zsh
tmux
terminator
rxvt-unicode
guake

# Browser
#qutebrowser
firefox

# Vim
gvim
the_silver_searcher
ctags

# i3
feh
xorg-xbacklight
picom
ranger
ffmpeg
xclip
xdotool

# Development
code
docker
docker-compose
aws-cli

# Other
vlc
mpv
tree
#python-pyqtwebengine
rofi
rofi-calc
lastpass-cli
pavucontrol
discord
i3blocks

# Bluetooth
blueman

# PDF Editor
evince
