#!/bin/bash

killall -q polybar
# polybar -r main &

# starts polybar in all monitors
TRAY_POS=right
for m in $(polybar -m | cut -d":" -f1); do
    MONITOR=$m TRAY_POS=$TRAY_POS polybar --config=$DOTFILES/scripts/polybar/config --reload main &
    TRAY_POS=none
done
