#!/bin/bash

menu="$1"
ROFI=$DOTFILES/scripts/rofi

if [ "$menu" = "music" ]; then
    rofi -modi "Music:$ROFI/music.sh" -show Music -theme $ROFI/music-launcher.rasi
elif [ "$menu" = "appmenu" ]; then
    rofi -show drun # -theme $ROFI/clean.rasi
# elif [ "$menu" = "powermenu" ]; then
#    rofi -modi "Powermenu:$ROFI/powermenu.sh" -show Powermenu -theme powermenu
#    rofi -modi "Powermenu:$ROFI/powermenu.sh" -show Powermenu -theme powermenu -location 3 -xoffset -24 -yoffset 70
fi
